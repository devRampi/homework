// Написать программу определения оценки студента по его рейтингу
let rating = Number(prompt('Введите рейтинг студента', ''));

function checkRating (a) {
	if (rating <0 || rating>100) {
		return('Введите значение от 0 до 100');
	}
	else if (rating < 20) {
		return('Рейтинг студента "F"');
	}
	else if (rating < 40) {
		return('Рейтинг студента "E"');
	}
	else if (rating < 60) {
		return('Рейтинг студента "D"');
	}
	else if (rating < 75) {
		return('Рейтинг студента "C"');
	}
	else if (rating < 90) {
		return('Рейтинг студента "B"');
	}
	else if (rating <= 100) {
		return('Рейтинг студента "A"');
	}	
}	
alert(checkRating());