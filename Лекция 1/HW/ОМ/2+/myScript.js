// Найти максимальный элемент массива
function findMaxElement () {
	var arr = [];
	var elementMass;
	var numOfElement = 1;
	var n = Number(prompt('Введите длину массива', ''));

	for(var i = 0; i <= n; i += 1) {
		elementMass = Number(prompt('Введите ' + numOfElement + ' элемент массива'));
		arr.push(elementMass);
		numOfElement += 1;
	}
	return Math.max(...arr);
}
console.log(findMaxElement());
