// Найти минимальный элемент массива
function findMinElement () {
	var arr = [];
	var elementMass;
	var numOfElement = 1;
	var n = Number(prompt('Введите длину массива', ''));


	for(var i = 0; i <= n; i += 1) {
		elementMass = Number(prompt('Введите ' + numOfElement + ' элемент массива'));
		arr.push(elementMass);
		numOfElement += 1;
	}
	return Math.min(...arr);
}
console.log(findMinElement());


/*
function findMinElement (myArr ) {
  return Math.min(...myArr);
}

let arr = [1,2,3,566,8,478,4,0,5];
let minElement = findMinElement(arr);
console.log(minElement); 
*/
