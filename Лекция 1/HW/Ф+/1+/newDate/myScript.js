// Получить строковое название дня недели по номеру дня
function getWeekDay(date) {
  let days = [
  'Воскресенье', 
  'Понедельник', 
  'Вторник', 
  'Среда', 
  'Четверг', 
  'Пятница',
  'Суббота'];

  return days[date.getDay()];
}

let date = new Date(2019, 10, 11); // 3 января 2014 года
alert( getWeekDay(date) ); // ПН